let h1 = document.getElementById("h1");


//////////////////////////////////////////
// console.log("Request data...");

// setTimeout(() => {
//   console.log("Preparing data...");

//   const data = {
//     server: "aws",
//     port: 2000,
//     status: "pending",
//   };

//   setTimeout(() => {
//     data.status = "success";
//     console.log("Data received:1 ", data);
//   }, 2000);
// }, 2000);
////////////////////////////////////////////

///////////////////////////////promise

// const getData = () => {
//   console.log("request data...");
//   const promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       console.log("preparing data");
//       const data = {
//         server: "aws",
//         port: 2000,
//         status: "pending",
//       };
//       // reject("data qaytmadi");
//       resolve(data);
//     }, 2000);
//   });
//   promise
//     .then((data) => {
//       data.status = "success";
//       // console.log(data);
//       if (data.status === "success") {
//         console.log("data received - ", data);
//       }
//     })
//     .catch((err) => console.log("rejected ", err));
//   return promise;
// };
//getData();
///////////////////////////////promise




/////////////////////////////// fetch
// const api = 'https://jsonplaceholder.typicode.com/users'

// const promise = fetch(api)
// const response = promise.then((response) => response.json())
// const data = response.then((data)=> console.log('data', data))
/////////////////////////////// fetch




//////////////////////////// async-await
const getData = async function () {
  try {
    const promise = await fetch("https://jsonplaceholder.typicode.com/users");
    const response = await promise.json();

    if (response.length) {
      console.log(response);

      setTimeout(() => {
        h1.style.width = `400px`;
        h1.textContent = `name: ${response[0].name}
        email: ${response[0].email} phoneNumber: ${response[0].phone}`;
      }, 3000);
    }
    if (response === undefined) {
      throw new Error("We couldn't get the data");
    }
  } catch (err) {
    console.log(err);
    console.log(response);
  }
};

getData();
//////////////////////////// async-await

