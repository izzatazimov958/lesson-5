const URL = "https://jsonplaceholder.typicode.com/users";

function getUsers(urlAddress) {
  return fetch(urlAddress).then((res) => res.json());
}

const ul = document.querySelector("ul");

getUsers(URL).then((data) => {
  data.forEach((user) => {
    const name = user.name;
    const email = user.email;

    const li = document.createElement("li");

    const spanName = document.createElement("span");
    const spanEmail = document.createElement("span");

    spanName.classList.add("user-name");
    spanEmail.classList.add("user-email");

    spanName.textContent = name;
    spanEmail.textContent = email;

    li.append(spanName, spanEmail);

    ul.append(li);
    console.log(ul);
  });
});

console.log(getUsers(URL));
